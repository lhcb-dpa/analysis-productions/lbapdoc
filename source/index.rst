.. LbAPDoc documentation master file, created by
   sphinx-quickstart on Fri May 13 17:14:04 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LHCb Analysis Productions!
=====================================

The goal of Analysis Productions (the :doc:`DPA-WP2 <dpa:wp2/index>` project) is to support user processing of data and simulation using the DIRAC transformation system.
It is an extension of the *WG productions* system to allow all users to easily submit ntupling jobs for Run 1, Run 2 and Run 3 data and simulation.

Completely new to Analysis Productions? Check out the :doc:`starterkit:first-analysis-steps/analysis-productions` Starterkit tutorial.


Getting Help
------------

Questions can be asked on the `“DPA WP2 Analysis Productions” mattermost
channel <https://mattermost.web.cern.ch/lhcb/channels/dpa-wp2-analysis-productions>`__.

Contributing
------------

You might have noticed these documentation pages are still under development.
Any contributions to these pages are very welcome!
The sources are available on CERN GitLab at `lhcb-dpa/analysis-productions/LbAPDoc <https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAPDoc>`__.


Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   .. user_guide/introduction
   user_guide/creating
   user_guide/yaml_sub_keys
   user_guide/testing
   user_guide/submitting
   user_guide/accessing_output

   user_guide/housekeeping
   user_guide/info-for-2024

.. toctree::
   :maxdepth: 2
   :caption: Developer Guide

   developer_guide/overview

.. toctree::
   :maxdepth: 1
   :caption: API Reference

   api_reference/lbapcommon
   api_reference/lbaplocal
   api_reference/lbapi
   api_reference/apd
   .. api_reference/lbapweb



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
