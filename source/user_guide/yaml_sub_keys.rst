YAML Configuration Sub-Keys
===========================

.. note::
    Keys marked with an asterisk are optional and can be omitted.

.. _input
``input``
---------------

.. _bk_query
``bk_query``
^^^^^^^^^^^^
+-------------------------------+----------------------+-----------------------------------------------------------------------+
|              Key              |      Type            |        Meaning                                                        |
+===============================+======================+=======================================================================+
| ``bk_query``                  | string               | The bookkeeping location of the desired input data.                   |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``n_test_lfns``\ \*           | int                  | The number of files to use as input to test jobs. Only to be used for |
|                               |                      | samples with very few output candidates.                              |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``sample_fraction``\ \*       | float                | The sampling fraction to use when sampling the input LFNs to use in   |
|                               |                      | the production. For example, ``0.1`` will sample 10% of input files.  |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``sample_seed``\ \*           | string               | The seed to use when sampling input LFNs. For example, ``HelloWorld`` |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``dq_flags``\ \*              | sequence of strings  | What quality of data to use. This can be set to any of ``BAD``,       |
|                               |                      | ``OK``, ``UNCHECKED`` or ``EXPRESS_OK`` (only for Runs 1 & 2).        | 
|                               |                      | Multiple can be used at once by writing them as a sequence of values. |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``extended_dq_ok``\ \*        | sequence of strings  | In addition to requiring data quality (DQ) ``OK``, extended DQ flags  |
|                               |                      | can be required such that runs without the specified subsystem DQ OK  | 
|                               |                      | flag being set are not included.                                      |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``runs``\ \*                  | sequence of integers | A sequence of data taking runs to use as input. This can either be    |
|                               |                      | written as a typical sequence or as A:B where runs from A to B        |
|                               |                      | inclusive will be used.                                               |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``input_plugin``\ \*          | string               | The input plugin setting, either ``default`` or ``by-run``.           |
|                               |                      | ``default==default``                                                  |
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``keep_running``\ \*          | bool                 | Whether to keep running on new data as it comes in.                   |
|                               |                      | ``default==False``                                                    |  
+-------------------------------+----------------------+-----------------------------------------------------------------------+
| ``smog2_state``\ \*           | string               | Gas injected in SMOG2, possible choices are: [Hydrogen, Deuterium,    |
|                               |                      | Helium, Nitrogen, Oxygen, Neon, Argon, Krypton, Xenon].               |
|                               |                      | 2 possible states: ``<Name>``, ``<Name>Unstable`` or both. The word.  |
|                               |                      | Unstable is appended if the gas injected pressure is not stable.      |  
+-------------------------------+----------------------+-----------------------------------------------------------------------+


Here is a full example showing a ``bk_query`` input using all optional keys:

.. code:: yaml

  job_name:
    input:
      bk_query: /some/MagUp/bookkeeping/path.DST
      n_test_lfns: 3
      dq_flags:
        - BAD
        - OK
      runs:
        - 269370
        - 269371
        - 269372
        # equivalent to 269370:269372
      input_plugin: by-run
      keep_running: True
      smog2_state:
        - Argon
        - ArgonUnstable

.. _job_name
``job_name``
^^^^^^^^^^^^
+-------------------------------+----------------------+----------------------------------------------------------------------+
|              Key              |      Type            |        Meaning                                                       |
+===============================+======================+======================================================================+
| ``job_name``                  | string               | The name of the job whose output should be the input of this job.    |
+-------------------------------+----------------------+----------------------------------------------------------------------+
| ``filetype``\ \*              | string               | The file type of the input file, for when your input job has         |
|                               |                      | multiple output files.                                               |
+-------------------------------+----------------------+----------------------------------------------------------------------+

Here is a full example showing a ``job_name`` input using all optional keys:

.. code:: yaml

  strip_job:
    bk_query: /some/MagUp/bookkeeping/path.DST
    options: strip.py

  tuple_job:
    input:
      job_name: strip_job
      filetype: DST
    options: tuple.py

.. _transform_ids
``transform_ids``
^^^^^^^^^^^^^^^^^
+-------------------------------+----------------------+----------------------------------------------------------------------+
|              Key              |      Type            |        Meaning                                                       |
+===============================+======================+======================================================================+
| ``transform_ids``             | sequence of integers | A sequence of transformation IDs to use as input file sources.       |
+-------------------------------+----------------------+----------------------------------------------------------------------+
| ``filetype``                  | string               | The file type of the input file, for when your input job has         |
|                               |                      | multiple output files.                                               |
+-------------------------------+----------------------+----------------------------------------------------------------------+
| ``n_test_lfns``\ \*           | int                  | The number of files to use as input to test jobs. Only to be used for|
|                               |                      | samples with very few output candidates.                             |
+-------------------------------+----------------------+----------------------------------------------------------------------+
| ``dq_flags``\ \*              | sequence of strings  | What quality of data to use. This can be set to any of ``BAD``,      |
|                               |                      | ``OK``, ``UNCHECKED`` or ``EXPRESS_OK`` (only for Runs 1 & 2).       | 
|                               |                      | Multiple can be used at once by writing them as a sequence of values.|
+-------------------------------+----------------------+----------------------------------------------------------------------+
| ``runs``\ \*                  | sequence of integers | A sequence of data taking runs to use as input. This can either be   |
|                               |                      | written as a typical sequence or as A:B where runs from A to B       |
|                               |                      | inclusive will be used.                                              |
+-------------------------------+----------------------+----------------------------------------------------------------------+

Here is a full example showing a ``transform_ids`` input using all optional keys:

.. code:: yaml

  job_name:
    input:
      transform_ids:
        - 1234
        - 5678
      filetype: DST
      n_test_lfns: 3
      dq_flags:
        - BAD
        - OK
      runs:
        - 269370
        - 269371
        - 269372
        # equivalent to 269370:269372

