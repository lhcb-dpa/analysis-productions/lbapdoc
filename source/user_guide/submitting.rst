Submitting
==========

Open a merge request for your branch to ``master`` using the GitLab web interface.


Merge requests
--------------

When creating your merge request:

1. Mark the Merge Request as **Draft:** until you're happy with the results of tests and are ready to submit

2. If your request will replace or supersede existing samples, see :doc:`housekeeping` to check what you need to do.

.. important::
    If the merge request supersedes a previous one, add a comment
    at the end of the header: ``(supersedes !<MR number>)``. Please state
    explicitly in the description if the superseded ntuples can be deleted
    immediately, or after the new merge request is finalised. 

.. note::
    If you require a new release of DaVinci add the “DV release”
    label to your merge request to notify the DaVinci maintainer. They will
    set a two-week deadline for new merge requests to be included in a new
    DaVinci release in order to collate new features. Do not hesitate to ask
    for a quicker release if there is urgency.


When ready:

-  Remove **Draft:** from the Merge Request title.
-  Assign the merge request to your working group's DPA/RTA liaison

To find your working group's DPA/RTA liaison, see `this
page. <https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/LHCbWGLiaisons>`__


Expert approval may be required in circumstances where the request is likely to put significant strain on LHCb's resources and/or could benefit from further optimisations.
For example:

* A bookkeeping path is processed more than once: it is usually possible to avoid doing this by e.g. configuring all necessary tupling algorithms in one DaVinci job.
* Reconstruction of MC (currently requires PPG approval): another (more extreme) case of duplicated processing - MC reconstruction should be carried out once, with results stored centrally for everybody to (re-)use.
* Output sizes too large: if an output exceeds a certain size (currently the limit is around 10 TB) then this may indicate an issue with the amount of entries or branches filled per event. (N.B. this will eventually be adapted to take into account the increasing size of input datasets).
* RAW processing: in a very small number of cases it will make sense to process RAW format input files, but in general only spruced output should be used.

Monitoring
----------

The status of a production can be monitored on the
`Analysis Productions
webpage <https://lhcb-analysis-productions.web.cern.ch/productions/>`__.
The previous monitoring on the `LHCbDirac web
portal <https://lhcb-portal-dirac.cern.ch/DIRAC/?url_state=1%7C*LHCbDIRAC.AnalysisProductions.classes.AnalysisProductions:,>`__
is being decommissioned.

Accessing output
----------------

See the next section :doc:`accessing_output` to figure out how to access your ntuples.
